import './registerServiceWorker'
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'

//dependencies
import VueNoty from 'vuejs-noty'
import Antd from 'ant-design-vue'

//style dependencies 
import 'ant-design-vue/dist/antd.css'
import 'vuejs-noty/dist/vuejs-noty.css'

Vue.config.productionTip = false

Vue.use(Antd)
Vue.use(VueNoty)

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
