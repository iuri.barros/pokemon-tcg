import axios from 'axios'

const apiRequest = axios.create({
  baseURL: process.env.VUE_APP_API_TCG
})

let apiTCG = { 
  //Get All Cards
  getAllCards: (q, page, pageSize, orderBy) => { 
    return apiRequest.get('/cards ', { params: { q, page, pageSize, orderBy} })
  },

  //Returns a specific card by id
  getCard: (cardId) => {
    return apiRequest.get(`/cards/${cardId}` )
  },

  //Get All Sets
  getAllSets: () => {
    return apiRequest.get('/sets ', { params: { q, page, pageSize, orderBy} })
  },

  //Returns a specific set by the set code
  getAllSet: (setId) => {
    return apiRequest.get(`/sets/${setId}`)
  },

  //Get All Types
  getTypes: () => {
    return apiRequest.get('/types ')
  },

  //Get All Sub Types
  getSubtypes: () => {
    return apiRequest.get('/subtypes ')
  },

  //Get All Super Types
  getSupertypes: () => {
    return apiRequest.get('/supertypes')
  },

  //Get All Rarities
  getRarities: () => {
    return apiRequest.get('/rarities ')
  }

}

export default apiTCG
