import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path:'/cards',
    name: 'Cards',
    component: () => import('../views/cards/cards.vue')
  },
  {
    path:'/card',
    name: 'Card',
    component: () => import('../views/cards/card.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
